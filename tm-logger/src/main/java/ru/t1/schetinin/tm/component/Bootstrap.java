package ru.t1.schetinin.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.listener.EntityListener;
import ru.t1.schetinin.tm.service.LoggerService;

import javax.jms.*;

@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private final static String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private final static String QUEUE = "LOGGER";

    @SneakyThrows
    public void run(@Nullable final String... args) {
        @NotNull final LoggerService loggerService = new LoggerService();
        @NotNull final EntityListener entityListener = new EntityListener(loggerService);
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}