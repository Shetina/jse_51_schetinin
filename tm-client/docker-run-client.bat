echo off
xcopy D:\TaskManager\jse_51_schetinin\.git .\.git /i /s /e /y
xcopy D:\TaskManager\jse_51_schetinin\tm-domain .\tm-domain /i /s /e /y
docker login
docker build -t shetina/tm-client .
docker push shetina/tm-client
rmdir /s /q .git
rmdir /s /q tm-domain
pause