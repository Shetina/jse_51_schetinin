echo off
xcopy D:\TaskManager\jse_51_schetinin\.git .\.git /i /s /e /y
xcopy D:\TaskManager\jse_51_schetinin\tm-domain .\tm-domain /i /s /e /y
xcopy D:\TaskManager\jse_51_schetinin\tm-logger .\tm-logger /i /s /e /y
docker login
docker build -t shetina/tm-server .
docker push shetina/tm-server
rmdir /s /q .git
rmdir /s /q tm-domain
rmdir /s /q tm-logger
pause